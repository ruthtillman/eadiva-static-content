# EADiva Static Content

Pulling together content from both EADiva sites, including page content and meta information.

## Meta Tags

* `title`: the title
* `element`: use "yes" when the page is an EAD element, use "deprecated" when the page is only EAD2002, use "ead3" when it's new in ead3.
* `url`: record the fuller post slug as the url

## Coding

### anchor / ID

```
### Attributes

### Child Elements {#subelements}

### DACS

### Examples

### Changes from EAD 2002 {#changes}
```

Hugo's Markdown processor parses a header's text as its anchor/ID. You can also add: `{#an-id}` *in line with a header* to create a custom ID.

For child elements, use: ` {#subelements}`

For changes from EAD 2002, use: ` {#changes}`

### Example code

Use triple backticks to enclose example code. Add "xml" after the first set of backticks to have the block parsed as XML.
