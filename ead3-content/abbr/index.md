---
title: "&lt;abbr&gt; Abbreviation"
element: yes
url: "abbr"

---

&lt;abbr&gt; is a generic element which encodes the shortened form of a word or phrase, including acronyms. The @expan attribute may be used to supply the full form of the word or words for indexing or searching, but it is not required.

Within [<conventiondeclaration>](https://eadiva.com/conventiondeclaration) it is used to specify the code for a thesaurus, controlled vocabulary, or other standard used in creating the EAD description. Best practices recommend using an authorized list of codes, such as the [MARC Description Convention Source Codes](http://www.loc.gov/standards/sourcelist/descriptive-conventions.html). It may not be repeated within <conventiondeclaration>, although it may be reused within any other parent element.

<abbr> may be used within [<abstract>](https://eadiva.com/abstract), [<addressline>](https://eadiva.com/addressline), [<archref>](https://eadiva.com/archref), [<author>](https://eadiva.com/author), [<bibref>](https://eadiva.com/bibref), [<citation>](https://eadiva.com/citation), [<container>](https://eadiva.com/container), [<conventiondeclaration>](https://eadiva.com/conventiondeclaration), [<date>](https://eadiva.com/date), [<datesingle>](https://eadiva.com/datesingle), [<didnote>](https://eadiva.com/didnote), [<dimensions>](https://eadiva.com/dimensions), [<edition>](https://eadiva.com/edition), [<emph>](https://eadiva.com/emph), [<entry>](https://eadiva.com/entry), [<event>](https://eadiva.com/event), [<fromdate>](https://eadiva.com/fromdate), [<head>](https://eadiva.com/head), [<head01>](https://eadiva.com/head01), [<head02>](https://eadiva.com/head02), [<head03>](https://eadiva.com/head03), [<item>](https://eadiva.com/item), [<label>](https://eadiva.com/label), [<localtypedeclaration>](https://eadiva.com/localtypedeclaration), [<materialspec>](https://eadiva.com/materialspec), [<num>](https://eadiva.com/num), [<p>](https://eadiva.com/p), [<part>](https://eadiva.com/part), [<physdesc>](https://eadiva.com/physdesc), [<physfacet>](https://eadiva.com/physfacet), [<physloc>](https://eadiva.com/physloc), [<publisher>](https://eadiva.com/publisher), [<quote>](https://eadiva.com/quote), [<ref>](https://eadiva.com/ref), [<sponsor>](https://eadiva.com/sponsor), [<subtitle>](https://eadiva.com/subtitle), [<titleproper>](https://eadiva.com/titleproper). [<todate>](https://eadiva.com/todate), [<unitdate>](https://eadiva.com/unitdate), [<unitid>](https://eadiva.com/unitid), and [<unittitle>](https://eadiva.com/unittitle).

### Attributes

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@expan** – not required. May be used to designate the expanded form of an abbreviation.
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

### Child Elements {#subelements}

&lt;abbr&gt; may only contain text.

### Examples

```xml
<conventiondeclaration>
  <abbr>DACS</abbr>
    <citation href="https://saa-ts-dacs.github.io/dacs/" lastdatetimeverified="2020-06-09T16:30:21Z" linktitle="DACS in HTML on GitHub" actuate="onload" show="new">Describing Archives: a Content Standard</citation>
  <descriptivenote>
    <p>This finding aid conforms to the University Archives' standards for DACS-compliant description, including all Required and Optimum fields.</p>
  </descriptivenote>
</conventiondeclaration>
```

```xml
<originalsloc>
  <head>Location of Originals</head>
  <p>File contains photocopies of the originals. Original newsletters are held by the <expan abbr="NARA">National Archives and Records Administration</expan> in accordance with <ref href="https://www.archives.gov/files/records-mgmt/rcs/schedules/independent-agencies/rg-0255/n1-255-92-004_sf115.pdf" linkrole="application/pdf"><abbr expan="National Archives and Records Administration">NARA</abbr> Request N1-255-92-004</ref>.</p>
</originalsloc>
```

### Changes from EAD 2002 {#changes}

&lt;abbr&gt; had only minor changes from EAD 2002, changing some parent elements and gaining attributes @lang and @script.
