---
title: "&lt;agenttype&gt; Agent Type"
element: ead3
url: "agenttype"
---

<agenttype> is a new element in EAD3 and a **required** child element of [<maintenanceevent>](https://eadiva.com/maintenanceevent) used to designate the type of agent who made the maintenance event occur. It should be used to specify whether it's a human or a machine (if an automated database routine is used to update the EAD file, for example).

<h3 id="attributes">Attributes</h3>

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema.
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).
* **@value** – **REQUIRED**. Values may be: human, machine, or unknown.

<h3 id="subelements">Child Elements</h3>

<agentype> may contain text. It may also just be used to encode the value as an empty element, see examples.

<h3 id="adjacent">Adjacent Elements</h3>

<agenttype> is preceded by the required [<eventdatetime>](https://eadiva.com/eventdatetime) and followed by the required [<agent>](https://eadiva.com/agent).

<h3 id="examples">Examples</h3>

```xml
<maintenancehistory>
  <maintenanceevent>
    <eventtype value="derived"/>
    <eventdatetime standarddatetime="2017-03-01T08:05:33Z">March 1 2017</eventdatetime>
    <agenttype value="machine"/>
    <agent>ead2-to-ead3.xslt</agent>
    <eventdescription>Conversion from EAD 2002 finding aid using XSL transformation.</eventdescription>
  </maintenanceevent>
  <maintenanceevent>
    <eventtype value="revised"/>
    <eventdatetime standarddatetime="2017-03-02T10:05:23Z">March 2 2017</eventdatetime>
    <agenttype value="human"/>
    <agent>Ruth Tillman</agent>
    <eventdescription>Conversion from EAD 2002 revised using updated DACS principles.</eventdescription>
  </maintenanceevent>
</maintenancehistory>
```