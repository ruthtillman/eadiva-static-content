---
title: "&lt;address&gt; Address"
element: yes
url: "address"
---

<address> is a wrapper element used to bundle [<addressline>](https://eadiva.com/addressline) elements with any kind of address information—postal, email, phone number, etc. The address may belong to the repository or publisher. The Library of Congress suggests storing repeated addresses externally, in another file or the XSLT so that they may be updated all at once.

&lt;address&gt; may be used within <a href="https://eadiva.com/publicationstmt">&lt;publicationstmt&gt;</a>, where it may be repeated, and <a href="https://eadiva.com/repository">&lt;repository&gt;</a>, where it may only be used once. See below for the use changes from EAD 2002.

<h3 id="attributes">Attributes</h3>

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

<h3 id="subelements">Child Elements</h3>

<address> must contain [<addressline>](https://eadiva.com/addressline) elements, which are used to encode the actual address.

<h3 id="examples">Examples</h3>

```xml
<publicationstmt>
	<publisher>Piecemaking University</publisher>
	<date normal="2020-06-08">June 8, 2020</date>
	<address>
		<addressline>University Archives</addressline>
		<addressline>Piecemaking University</addressline>
		<addressline>Lancaster, PA 17603</addressline>
		<addressline localtype="phone">+1-717-555-1313</addressline>
		<addressline localtype="email">archivist@piecemaking.edu</addressline>
	</address>
</publicationstmt>
```

<h3 id="changes">Changes from EAD 2002</h3>

While its actual use stayed the same between EAD 2002 and EAD3, <address> was restricted to only be used within <repository> and <publicationstmt>. In EAD 2002 it could also be used within: <accessrestrict>, <accruals>, <acqinfo>, <altformavail>, <appraisal>, <arrangement>, <bibliography>, <bioghist>, <blockquote>, <controlaccess>, <custodhist>, <daodesc>, <descgrp>, <div>, <dsc>, <dscgrp>, <entry>, <event>, <extref>, <extrefloc>, <fileplan>, <index>, <item>, <note>, <odd>, <originalsloc>, <otherfindaid>, <p>, <phystech>, <prefercite>, <processinfo>, <ref>, <refloc>, <relatedmaterial>, <scopecontent>, <separatedmaterial>, <titlepage>, and <userestrict>. It gained the attributes @lang and @script.