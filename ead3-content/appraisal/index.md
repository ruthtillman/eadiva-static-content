---
title: "&lt;appraisal&gt; Appraisal"
element: yes
url: "appraisal"

---

&lt;appraisal&gt; is used to encode information about the process of determining the archival value and disposition of the records based upon their current administrative, legal, and fiscal use; their evidential, intrinsic, and informational value; their arrangement and condition; and their relationship to other records. It does <em>not</em> pertain to the monetary value of the records.

<appraisal> is one of the elements which may be used within [<archdesc>](https://eadiva.com/archdesc), [<c>](https://eadiva.com/c), [<c01> ... <c12>](https://eadiva.com/c01), and itself.

<h3 id="attributes">Attributes</h3>

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema.
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@localtype** – not required. This attribute may be used within a number of elements. Its use and values are not defined by the schema and may be defined locally.
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

<h3 id="subelements">Child Elements</h3>

<appraisal> may contain an optional [<head>](https://eadiva.com/head) element. It must contain one or more of the following: [<blockquote>](https://eadiva.com/blockquote), [<chronlist>](https://eadiva.com/chronlist), [<list>](https://eadiva.com/list), [<p>](https://eadiva.com/p), [<table>](https://eadiva.com/table), and further <appraisal> elements.

<h3 id="DACS">DACS</h3>

See [DACS Section 5.3, Appraisal/destruction/scheduling information](http://www2.archivists.org/standards/DACS/part_I/chapter_5/3_appraisal_destruction_and_scheduling_information). Added value. (DACS 2013, pp.66-67)

<h3 id="examples">Examples</h3>

```xml
<appraisal>
  <head>Appraisal of Materials</head>
  <p>The initial records collection came to the University Archives as part of a records management initiative in 1989. During processing, University Archivist Wilhelmena Ransom discarded event fliers, duplicate copies of meeting notes, and department publications. Working with the records manager, she drafted the schedule which became schedule UA-113 to guide further materials transfer.</p>
    <p>If records received in an annual transfer (per schedule UA-113) do not fit within existing series, the current University Archivist evaluates them for the potential creation of a new series or subseries.</p>
    <p>Duplicate materials are discarded during processing.</p>
</appraisal>
```

<h3 id="changes">Changes from EAD 2002</h3>
	
<appraisal> gained attributes @lang and @script. @type changed to @localtype. It lost child elements <address> and <note>.