---
title: "&lt;agencyname&gt; Agency Name"
element: ead3
url: "agencyname"
---

<agencyname> is a new element in EAD3 and a **required** child element of [<maintenanceagency>](https://eadiva.com/maintenanceagency) that provides the name of the agency responsible for the creation, maintenance, or dissemination of the EAD finding aid. Use in combination with [<agencycode>](https://eadiva.com/agencycode) and possibly [<otheragencycode>](https://eadiva.com/otheragencycode). It may be repeated to provide the name in multiple languages (particularly in multi-lingual countries). The @lang attribute may be used to distinguish these.

<h3 id="attributes">Attributes</h3>

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema.
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@localtype** – not required. This attribute may be used within a number of elements. Its use and values are not defined by the schema and may be defined locally.
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

<h3 id="subelements">Child Elements</h3>

<agencyname> may contain text.

<h3 id="adjacent">Adjacent Elements</h3>

<agencyname> may be preceded by an optional <otheragencycode> and followed by an optional <descriptivenote>.

<h3 id="examples">Examples</h3>

```xml
<maintenanceagency>
  <agencycode>US-Papmua</agencycode>
  <otheragencycode localtype="quiltstan">PMU</otheragencycode>
  <agencyname>Piecemaking University Archives</agencyname>
</maintenanceagency>
```