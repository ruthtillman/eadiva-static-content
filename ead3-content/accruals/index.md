---
title: "&lt;accruals&gt; Accruals"
element: yes
url: "accruals"
---

&lt;accruals&gt; encodes information about anticipated additions to the materials being described, including quantity and frequency. It can also be used to indicate no additions are expected.

<accruals> is one of the elements which may be used within [<archdesc>](https://eadiva.com/archdesc), [<c>](https://eadiva.com/c), [<c01> ... <c12>](https://eadiva.com/c01), and itself.

### Attributes

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema.
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@localtype** – not required. This attribute may be used within a number of elements. Its use and values are not defined by the schema and may be defined locally.
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

### Child Elements {#subelements}

<accruals> may contain an optional [<head>](https://eadiva.com/head) element. It must contain one or more of the following: [<blockquote>](https://eadiva.com/blockquote), [<chronlist>](https://eadiva.com/chronlist), [<list>](https://eadiva.com/list), [<p>](https://eadiva.com/p), [<table>](https://eadiva.com/table), and further <accruals> elements.

### DACS

See [DACS Section 5.4, Accruals](http://www2.archivists.org/standards/DACS/part_I/chapter_5/4_accruals). Added value. (DACS 2013, p.68)

### Examples

```xml
<accruals>
  <head>Accruals</head>
  <p>Grant applications and paperwork are retained by the department until 7 years
    from the grant's completion. Unsuccessful applications are then shredded. The
    successful application and its paperwork are transferred to the University
    Archives</p>
</accruals>
```

```xml
<accruals>
  <head>Accruals</head>
  <p>The department minutes for each school year are acquired in June, following the conclusion of Maymester.</p>
</accruals>
```

### Changes from EAD 2002 {#changes}

&lt;accruals&gt; gained attributes @lang and @script. @type changed to @localtype. It lost child elements &lt;address&gt; and &lt;note&gt;.
