---
title: "&lt;acqinfo&gt; Acquisition Information"
element: yes
url: "acqinfo"

---

<acqinfo> encodes the immediate source of the described materials and circumstances under which they were received. It includes donations, transfers, purchases, and deposits. It is comparable to ISAD(G) data element 3.2.4 and MARC21 field 541\. The [<p>](https://eadiva.com/p) may contain such child elements as [<persname>](https://eadiva.com/persname), [<corpname>](https://eadiva.com/corpname), [<date>](https://eadiva.com/date), and [<num>](https://eadiva.com/num) which may better help describe the acquisition of the materials.

If some of the items acquired were physically separated from the materials being described, [<separatedmaterial>](https://eadiva.com/separatedmaterial) should be used to describe them elsewhere. [<acqinfo>](https://eadiva.com/acqinfo) should not be confused with [<custodhist>](https://eadiva.com/custodhist), which can be used for information about the chain of ownership _before_ materials came into the possession of the immediate source of acquisition.

<acqinfo> is one of the elements which may be used within [<archdesc>](https://eadiva.com/archdesc), [<c>](https://eadiva.com/c), [<c01> ... <c12>](https://eadiva.com/c01), and itself.

### Attributes

* **@altrender** - not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@localtype** – not required. This attribute may be used within a number of elements. Its use and values are not defined by the schema and may be defined locally.
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

### Child Elements {#subelements}

<acqinfo> may contain an optional [<head>](https://eadiva.com/head) element. It must contain one or more of the following: [<blockquote>](https://eadiva.com/blockquote), [<chronlist>](https://eadiva.com/chronlist), [<list>](https://eadiva.com/list), [<p>](https://eadiva.com/p), [<table>](https://eadiva.com/table), and further <acquinfo> elements.

### DACS

See [DACS Section 5.2, Immediate source of acquisition](http://www2.archivists.org/standards/DACS/part_I/chapter_5/2_immediate_source_of_acquisition). Added value. (DACS 2013, pp.64-65)

### Examples

```xml
<acqinfo>
  <head>Acquisition History</head>
  <p>Records from 1978 to 1988 were acquired in 1989 from contemporaneous and previous department faculty and the department secretary.</p>
  <p>Materials are transferred annually from the Quilting Technologies department in accordance with University Records Schedule UA-113. Because all files are now electronic, the department retains copies of these records for ongoing reference.</p>
  <p>A 2006 donation of papers and two hard drives by <persname normal="Russel, Martha A. (Martha Anita), 1941-2010"> <part>Dr. Martha Russel</part></persname> was used to fill gaps in the record.</p>
</acqinfo>
```

### Changes from EAD 2002 {#changes}

<acqinfo> gained attributes @lang and @script. @type changed to @localtype. It lost child elements <address> and <note>. It used to be available within <custodhist> but isn't any more.