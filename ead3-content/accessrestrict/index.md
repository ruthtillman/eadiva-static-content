---
title: "&lt;accessrestrict&gt; Conditions Governing Access"
element: yes
url: "accessrestrict"
---

&lt;accessrestrict&gt; is a wrapper element which contains information about conditions that affect the availability of the materials being described. These conditions may be based on location, physical condition, and restrictions imposed by another party. It may also indicate that there are no restrictions on the described materials. It is comparable to ISAD(G) data element 3.4.1 and MARC21 field 506.

This should not be confused with [<userestrict>](https://eadiva.com/userestrict), which designates information about limitations on the use of accessed materials or [<legalstatus>](https://eadiva.com/legalstatus), which encodes the statutorily-defined status of the materials.

<accessrestrict> is one of the elements which may be used within [<archdesc>](https://eadiva.com/archdesc), [<c>](https://eadiva.com/c), [<c01> ... <c12>](https://eadiva.com/c01), and itself.

### Attributes

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema.
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@localtype** – not required. This attribute may be used within a number of elements. Its use and values are not defined by the schema and may be defined locally.
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

### Child Elements {#subelements}

<accessrestrict> may contain an optional [<head>](https://eadiva.com/head) element. It must contain one or more of the following: [<blockquote>](https://eadiva.com/blockquote), [<chronlist>](https://eadiva.com/chronlist), [<list>](https://eadiva.com/list), [<p>](https://eadiva.com/p), [<table>](https://eadiva.com/table), and further <accessrestrict> elements.

### DACS

See [DACS Section 4.1, Conditions governing access](http://www2.archivists.org/standards/DACS/part_I/chapter_4/1_conditions_governing_access) (**Required**) and [DACS Section 4.2, Physical access](http://www2.archivists.org/standards/DACS/part_I/chapter_4/2_physical_access) (Added value). (DACS 2013, pp.51-54)

### Examples

Some examples of how &lt;accessrestrict&gt; may be used:

```xml
<accessrestrict>
  <head>Restrictions on Access</head>
  <p>The collection is open for reearch. The following series require review by the archivist to redact protected information:</p>
  <list listtype="unordered">
  <item>Meeting Notes (for records less than 20 years old)</item>
  <item>Grants</item>
  </list>
</accessrestrict>
```

### Changes from EAD 2002 {#changes}

&lt;accessrestrict&gt; gained @lang and @script. @type changed to @localtype. It lost child elements: &lt;address&gt;,&lt;legalstatus&gt;, and &lt;note&gt;.
