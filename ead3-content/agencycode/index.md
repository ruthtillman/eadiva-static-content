---
title: "&lt;agencycode&gt; Agency Code"
element: ead3
url: "agencycode"
---

<agencycode> is a new element in EAD3 and an optional child element of [<maintenanceagency>](https://eadiva.com/maintenanceagency), used to supply a code for the agency responsible for the creation, maintenance, or dissemination _of the EAD finding aid_. For best practices, use the format of International Standard Identifier for Libraries and Related Organizations (ISO 15511). The code is composed of a prefix, a dash, and an identifier. For specific codes, see [the Library of Congress's code search](http://www.loc.gov/marc/organizations/).

When used in combination with [<recordid>](https://eadiva.com/recordid), <agencycode> can provide a globally unique identifier for the finding aid. The agency's name should be encoded in [<agencyname>](https://eadiva.com/agencyname) with [<otheragencycode>](https://eadiva.com/otheragencycode) used to designate any non-standard codes.

<h3 id="attributes">Attributes</h3>

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema.
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@localtype** – not required. This attribute may be used within a number of elements. Its use and values are not defined by the schema and may be defined locally.
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

<h3 id="subelements">Child Elements</h3>

<agencycode> may contain text.
<h3 id="adjacent">Adjacent Elements</h3>

<agencycode> may be followed by an optional [<otheragencycode>](https://eadiva.com/otheragencycode), then the required [<agencyname>](https://eadiva.com/agencyname).

<h3 id="examples">Examples</h3>

```xml
<maintenanceagency>
  <agencycode>US-Papmua</agencycode>
  <otheragencycode localtype="quiltstan">PMU</otheragencycode>
  <agencyname>Piecemaking University Archives</agencyname>
</maintenanceagency>
```