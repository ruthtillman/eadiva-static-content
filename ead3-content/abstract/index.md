---
title: "&lt;abstract&gt; Abstract"
element: yes
url: "abstract"

---

A very brief summary of the materials being described, used primarily to encode bits of biographical or historical information about the creator and abridged statements about the scope, content, arrangement, or other descriptive details about the archival unit or one of its components. It is used within [<did>](https://eadiva.com/did). It may be extracted from other fields, such as [<bioghist>](https://eadiva.com/bioghist) and [<scopecontent>](https://eadiva.com/scopecontent). Within a [<c>](https://eadiva.com/c/) -> [<did>](https://eadiva.com/), it indicates information specific to that component.

### Attributes

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema. In particular, you may want to use MARC equivalents summary note (520$a) or biographical or historical data (545$a).
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@label** – not required. This can be used when a meaningful display label for an element can't be derived by the stylesheet from its name. It is available in all <did> subelements.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@localtype** – not required. This attribute may be used within a number of elements. Its characteristics depend on the element to which it applies. There is no list of specific values for @localtype within <abstract>, but it may be used for encoding information.
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

### Child Elements {#subelements}

<abstract> may contain text and any of the following elements: [<abbr>](https://eadiva.com/abbr), [<corpname>](https://eadiva.com/corpname), [<date>](https://eadiva.com/date), [<emph>](https://eadiva.com/emph), [<expan>](https://eadiva.com/expan), [<famname>](https://eadiva.com/famname), [<footnote>](https://eadiva.com/footnote), [<foreign>](https://eadiva.com/foreign), [<function>](https://eadiva.com/function), [<genreform>](https://eadiva.com/genreform), [<geogname>](https://eadiva.com/geogname), [<lb />](https://eadiva.com/lb), [<name>](https://eadiva.com/name), [<num>](https://eadiva.com/num), [<occupation>](https://eadiva.com/occupation), [<persname>](https://eadiva.com/persname), [<quote>](https://eadiva.com/quote), [<ptr/>](https://eadiva.com/ptr), [<ref>](https://eadiva.com/ref), [<subject>](https://eadiva.com/subject), and [<title>](https://eadiva.com/title).

### Examples

```xml
<did>
  <unittitle>Department Minutes</unittitle>
  <abstract>Regular department meetings are held in September-December and February-April of each school year. Some years include minutes of additional meetings. Minutes for the years 1983 and 1986 were not retained by anyone in the department.</abstract>
</did>
</pre>
```

### Changes from EAD 2002 {#changes}

<abstract> has gained @script attribute and <foreign> child element. It may no longer occur within [<archref>](https://eadiva.com/archref). It loses <archref>, <bibref>, and <linkgrp> as child elements.
