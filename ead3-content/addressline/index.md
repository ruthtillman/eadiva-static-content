---
title: "&lt;addressline&gt; Address Line"
element: yes
url: "address"
---

<addressline> is a generic element used to encode a single line of an address (postal, email, telephone, etc.). It occurs within [<address>](https://eadiva.com/address) and may be repeated as many times as necessary to enter all the pertinent information.

<h3 id="attributes">Attributes</h3>

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from ISO 639-2b.
* **@localtype** – not required. This attribute may be used within a number of elements. Its characteristics depend on the element to which it applies. There is no list of specific values for @localtype within <addressline>, but it may be used for encoding information about the type of address line: email, phone, street, city, etc..
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from ISO 15924.

<h3 id="subelements">Child Elements</h3>

<addressline> most commonly contains text and may also contain [<abbr>](https://eadiva.com/abbr), [<emph>](https://eadiva.com/emph), [<expan>](https://eadiva.com/expan), [<foreign>](https://eadiva.com/foreign), [<lb />](https://eadiva.com/lb), [<ptr/>](https://eadiva.com/ptr), and [<ref>](https://eadiva.com/ref).

<h3 id="examples">Examples</h3>

```xml
<publicationstmt>
	<publisher>Piecemaking University</publisher>
	<date normal="2020-06-08">June 8, 2020</date>
	<address>
		<addressline>University Archives</addressline>
		<addressline>Piecemaking University</addressline>
		<addressline>Lancaster, PA 17603</addressline>
		<addressline localtype="phone">+1-717-555-1313</addressline>
		<addressline localtype="email">archivist@piecemaking.edu</addressline>
	</address>
</publicationstmt>
```

<h3 id="changes">Changes from EAD 2002</h3>

<addressline> gained the attributes @lang, @localtype, and @script. It lost child element <extptr> and gained <abbr>, <expan>, <foreign>, and <ref>.