---
title: "&lt;altformavail&gt; Alternative Form Available"
element: yes
url: "altformavail"

---

<altformavail> encodes information about copies of the materials being described. It should include the type of alternative form, significant control numbers, location, and source for ordering copies if available. If the described materials _are_ copies, [<originalsloc>](https://eadiva.com/originalsloc) should be used to encode information about the existence, location, and availability of the originals. Copied materials may also have <altformavail>, if alternative forms such as photocopies, and digital or microfilm reproductions are available.

<altformavail> is one of the elements which may be used within [<archdesc>](https://eadiva.com/archdesc), [<c>](https://eadiva.com/c), [<c01> ... <c12>](https://eadiva.com/c01), and itself.

<h3 id="attributes">Attributes</h3>

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema.
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@localtype** – not required. This attribute may be used within a number of elements. Its characteristics depend on the element to which it applies. There is no list of specific values for @localtype within <altformavail>, but it may be used for encoding information about the type of alternate form available.
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

<h3 id="subelements">Child Elements</h3>

<altformavail> may contain an optional [<head>](https://eadiva.com/head) element. It must contain one or more of the following: [<blockquote>](https://eadiva.com/blockquote), [<chronlist>](https://eadiva.com/chronlist), [<list>](https://eadiva.com/list), [<p>](https://eadiva.com/p), [<table>](https://eadiva.com/table), and further <altformavail> elements.

### DACS

See [DACS Section 6.2, Existence/location of copies](http://www2.archivists.org/standards/DACS/part_I/chapter_6/2_existence_and_location_of_copies). Added value. (DACS 2013, pp.71-72)

<h3 id="examples">Examples</h3>

```xml
<altformavail>
  <head>Streaming Video</head>
  <p>Recordings of some symposia and colloquia are available on the department's <ref show="new" href="https://youtube.com/quilttech/colloquia" actuate="onload">YouTube page</ref>.</p>
</altformavail>
```

<h3 id="changes">Changes from EAD 2002</h3>
	
<altformavail> gained attributes @lang and @script. @type changed to @localtype. It lost child elements <address> and <note>.