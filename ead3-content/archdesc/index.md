---
title: "&lt;archdesc&gt; Archival Description"
element: yes
url: "archdesc"
---

This major, **required** element serves as the wrapper for the bulk of the EAD document. It describes the "content, context, and extent of a _body_ of archival materials, including administrative and supplemental information that facilitates use of the materials." It follows the [<control>](https://eadiva.com/control) section within [<ead>](https://eadiva.com/ead).

<h3 id="attributes">Attributes</h3>

* **@altrender** – not required. Use if the content of the element should be displayed or printed differently than the rendering established in a style sheet for other occurrences of the element.
* **@audience** – not required. Use to set whether the element's contents will be visible to external users or to internal ones. Possible values are: "internal" and "external."
* **@base** – not required. May contain a URI to create a base URI used to resolve any relative URIs used within the child elements, if the base URI differs from the base URI of the EAD instance.
* **@encodinganalog** – not required. May contain information to map this tag to a particular element in another schema.
* **@id** – not required. Creates an ID for element. Can be used for linking.
* **@lang** – not required. Three-letter code that indicates the language in which the element's contents were written. It should come from [ISO 639-2b](http://www.loc.gov/standards/iso639-2/php/code_list.php).
* **@level** – **required.** You _must_ designate whether this is a class, collection, file, fonds, item, otherlevel, recordgrp, series, subfonds, subgrp, or subseries.
* **@localtype** – not required. This attribute may be used within a number of elements. Within <archdesc> it may be used to set a token (NMTOKEN) to describe a local value for the type.
* **@otherlevel** – if you set the @level attribute to "otherlevel," because your repository has a different term for it, you must specify the other term here.
* **@relatedencoding** – not required. If _only_ the archdesc element (not the entire finding aid) is going to be mapped to a particular standard such as MARC or Dublin Code, this is where one would specify it.
* **@script** – not required. Four-letter code that indicates the script in which the element's contents were written. It should come from [ISO 15924](http://www.unicode.org/iso15924/iso15924-codes.html).

<archdesc> only has one required attribute, @level. This attribute is comparable to ISAD(G) data element 3.1.4 and MARC field 351 subfield c and required by DACS for best practices (as well as by the schema).

<h3 id="subelements">Child Elements</h3>

<archdesc> must contain a [<did>](https://eadiva.com/did), descriptive information element to provide a basic description of the materials. This must occur before other elements can be used.

Subsequently, one may use any or all of: [<accessrestrict>](https://eadiva.com/accessrestrict), [<accruals>](https://eadiva.com/accruals), [<acqinfo>](https://eadiva.com/acqinfo), [<altformavail>](https://eadiva.com/altformavail), [<appraisal>](https://eadiva.com/appraisal), [<arrangement>](https://eadiva.com/arrangement), [<bibliography>](https://eadiva.com/bibliography), [<bioghist>](https://eadiva.com/bioghist), [<controlaccess>](https://eadiva.com/controlaccess), [<custodhist>](https://eadiva.com/custodhist), [<dsc>](https://eadiva.com/dsc), [<fileplan>](https://eadiva.com/fileplan), [<index>](https://eadiva.com/index), [<legalstatus>](https://eadiva.com/legalstatus), [<odd>](https://eadiva.com/odd), [<originalsloc>](https://eadiva.com/originalsloc), [<otherfindaid>](https://eadiva.com/otherfindaid), [<phystech>](https://eadiva.com/phystech), [<prefercite>](https://eadiva.com/prefercite), [<processinfo>](https://eadiva.com/processinfo), [<relatedmaterial>](https://eadiva.com/relatedmaterial), [<relations>](https://eadiva.com/relations), [<scopecontent>](https://eadiva.com/scopecontent), [<separatedmaterial>](https://eadiva.com/separatedmaterial), and [<userestrict>](https://eadiva.com/userestrict).

List view:

* [**<did>**](https://eadiva.com/did)
  * [<abstract>](https://eadiva.com/abstract) (may be repeated)
  * [<container>](https://eadiva.com/container) (may be repeated)
  * [<dao>](https://eadiva.com/dao) (may be repeated)
  * [<daoset> (may be repeated)](https://eadiva.com/daoset)
  * [<didnote> (may be repeated)](https://eadiva.com/didnote)
  * [<langmaterial> (may be repeated)](https://eadiva.com/langmaterial)
    * [_<language>_](https://eadiva.com/language)\*\* (may be repeated and used in tandem with <languageset>)
    * [_<languageset>_](https://eadiva.com/languageset)\*\* (may be repeated and used in tandem with <language>)
      * [**<language>**](https://eadiva.com/language)\* (may be repeated)
      * [**<script>**](https://eadiva.com/script)\* (may be repeated)
      * [<descriptivenote>](https://eadiva.com/descriptivenote)
    * [<descriptivenote>](https://eadiva.com/descriptivenote)
  * [<materialspec>](https://eadiva.com/materialspec) (may be repeated)
  * [<origination>](https://eadiva.com/origination) (may be repeated)
      * [_<corpname>_](https://eadiva.com/corpname)\*\* (may be repeated)
      * [_<famname>_](https://eadiva.com/famname)\*\* (may be repeated)
      * [_<name>_](https://eadiva.com/name)\*\* (may be repeated)
      * [_<persname>_](https://eadiva.com/persname)\*\* (may be repeated)
  * [<physdescset>](https://eadiva.com/physdescset) (may be repeated)
      * [**<physdescstructured>**](https://eadiva.com/physdescstructured)\*
      * [**<physdescstructured>**](https://eadiva.com/physdescstructured)\*\* (must contain at least two, may contain infinite)
  * [<physdesc>](https://eadiva.com/physdesc) (may be repeated)
  * [<physdescstructured>](https://eadiva.com/physdescstructured) (may be repeated)
    * [**<quantity>**](https://eadiva.com/quantity)\*
    * [**<unittype>**](https://eadiva.com/unittype)\*
    * [<physfacet>](https://eadiva.com/physfacet) (may be repeated)
    * [<dimensions>](https://eadiva.com/dimensions) (may be repeated)
    * [<descriptivenote>](https://eadiva.com/descriptivenote)
  * [<physloc>](https://eadiva.com/physloc) (may be repeated)
  * [<repository>](https://eadiva.com/repository) (may be repeated)
    * [**<corpname>**](https://eadiva.com/corpname)\*\* (may be repeated)
    * [**<famname>**](https://eadiva.com/famname)** (may be repeated)
    * [**<name>**](https://eadiva.com/name)\*\* (may be repeated)
    * [**<persname>**](https://eadiva.com/persname)\*\* (may be repeated)
    * [<address>](https://eadiva.com/address)
  * [<unitdate>](https://eadiva.com/unitdate) (may be repeated)
  * [<unitdatestructured>](https://eadiva.com/unitdatestructured) (may be repeated)
    * one of: [<datesingle>](https://eadiva.com/datesingle), [<daterange>](https://eadiva.com/daterange), [<dateset>](https://eadiva.com/dateset)
    * [<unitid>](https://eadiva.com/unitid) (may be repeated)
    * [<unittype>](https://eadiva.com/unittype) (may be repeated)
* [<accessrestrict>](https://eadiva.com/accessrestrict) (may be repeated)
* [<accruals>](https://eadiva.com/accruals) (may be repeated)
* [<acqinfo>](https://eadiva.com/acqinfo) (may be repeated)
* [<altformavail>](https://eadiva.com/altformavail) (may be repeated)
* [<appraisal>](https://eadiva.com/appraisal) (may be repeated)
* [<arrangement>](https://eadiva.com/arrangement) (may be repeated)
* [<bibliography>](https://eadiva.com/bibliography) (may be repeated)
* [<bioghist>](https://eadiva.com/bioghist) (may be repeated)
* [<controlaccess>](https://eadiva.com/controlaccess) (may be repeated)
* [<custodhist>](https://eadiva.com/custodhist) (may be repeated)
* [<fileplan>](https://eadiva.com/fileplan) (may be repeated)
* [<index>](https://eadiva.com/index) (may be repeated)
  * A variety of generic elements, then:
  * [<listhead>](https://eadiva.com/listhead)
    * [<head01>](https://eadiva.com/head01/)
    * [<head02>](https://eadiva.com/head02/)
    * [<head03>](https://eadiva.com/head03/)
    * [_<index>_](https://eadiva.com/index)
    * [_<indexentry>_](https://eadiva.com/indexentry)
      * **One of:**\*
        * any **one** element listed under [access](#access) below, _or_
        * [<namegrp>](https://eadiva.com/namegrp)
          * **at least two elements** listed under [access](https://eadiva.com/elements/#access) below
      * An optional one of:
        * [<ptr/>](https://eadiva.com/ptr)
        * [<ref>](https://eadiva.com/ref)
        * [<ptrgrp>](https://eadiva.com/ptrgrp)
      * [<indexentry>](https://eadiva.com/indexentry) (may be repeated)
* [<legalstatus>](https://eadiva.com/legalstatus) (may be repeated)
* [<odd>](https://eadiva.com/odd) (may be repeated)
* [<originalsloc>](https://eadiva.com/originalsloc) (may be repeated)
* [<otherfindaid>](https://eadiva.com/otherfindaid) (may be repeated)
* [<phystech>](https://eadiva.com/phystech) (may be repeated)
* [<prefercite>](https://eadiva.com/prefercite) (may be repeated)
* [<processinfo>](https://eadiva.com/processinfo) (may be repeated)
* [<relatedmaterial>](https://eadiva.com/relatedmaterial) (may be repeated)
* [<relations>](https://eadiva.com/relations) (may be repeated)
  * [<relationentry>](https://eadiva.com/relationentry) (may be repeated)
  * [<objectxmlwrap>](https://eadiva.com/objectxmlwrap)
  * One of: [<datesingle>](https://eadiva.com/datesingle), [<daterange>](https://eadiva.com/daterange), [<dateset>](https://eadiva.com/dateset).
  * [<geogname>](https://eadiva.com/geogname)
  * [<descriptivenote>](https://eadiva.com/descriptivenote)
* [<scopecontent>](https://eadiva.com/scopecontent) (may be repeated)
* [<separatedmaterial>](https://eadiva.com/separatedmaterial) (may be repeated)
* [<userestrict>](https://eadiva.com/userestrict) (may be repeated)
* [<dsc>](https://eadiva.com/dsc)*** (may be repeated) (must be used if <c> elements will be used)
  * Any or all of these elements in any combination:
  * [<head>](https://eadiva.com/head) (may be repeated)
  * [<blockquote>](https://eadiva.com/blockquote) (may be repeated)
  * [<chronlist>](https://eadiva.com/chronlist) (may be repeated)
  * [<list>](https://eadiva.com/list) (may be repeated)
  * [<p>](https://eadiva.com/p) (may be repeated)
  * [<table>](https://eadiva.com/table) (may be repeated)
  * Possibly followed by:
  * [<thead>](https://eadiva.com/thead)
  * [<c>](https://eadiva.com/c) or
  * [<c01>](https://eadiva.com/c01)
    * [<c02>](https://eadiva.com/c02)
      * [<c03>](https://eadiva.com/c03)
        * etc. to [<c12>](https://eadiva.com/c12)

### Adjacent Elements {#adjacent}

<archdesc> is preceded by [<control>](https://eadiva.com/control) and is the final element in the sequence

<h3 id="DACS">DACS</h3>

Use [DACS Section 1.](http://www2.archivists.org/standards/DACS/part_I/chapter_1) Level of description for the @level attribute. (DACS 2013, pp.7-11)

<h3 id="examples">Examples</h3>

```xml
<archdesc level="collection">
  <did>
    <unittitle>Quilting Technologies Department Records</unittitle>
    <unitid>QD-011</unitid>
    <unitdate normal="1978/2020" unitdatetype="inclusive">1978-2020</unitdate>
    <unitdatestructured certainty="circa" unitdatetype="inclusive">
      <daterange>
        <fromdate notbefore="1975" standarddate="1978">1978, no earlier than 1975</fromdate>
        <todate standarddate="2020">2020</todate>
      </daterange>
  </unitdatestructured>
  <origination>
    <corpname encodinganalog="110" relator="creator" source="lcnaf"><part>Quilting Technologies Department</part><part>Piecemaking University</part></corpname>
  </origination>
  <physdesc>20 linear feet</physdesc>
  <physdesc>793 GB</physdesc>
  <physdescset coverage="whole" parallel="false">
    <physdescstructured coverage="part" physdescstructuredtype="spaceoccupied">
      <quantity>20</quantity>
      <unittype>linear feet</unittype>
    </physdescstructured>
    <physdescstructured coverage="part" physdescstructuredtype="carrier">
      <quantity>40</quantity>
      <unittype>boxes</unittype>
    </physdescstructured>
    <physdescstructured coverage="part" physdescstructuredtype="spaceoccupied">
      <quantity>793</quantity>
      <unittype>GB</unittype>
    </physdescstructured>
    <physdescstructured coverage="part" physdescstructuredtype="carrier">
      <quantity>2</quantity>
      <unittype>hard drives</unittype>
      <physfacet>labeled with printed sticker</physfacet>
      <dimensions localtype="width" unit="inches">3.5</dimensions>
        <descriptivenote>
          <p>Labels on hard drives were applied by department before transfer to University Archives. If hard drives are used incorrectly, labels may melt and damage equipment. Use Archives-provided chassis to safely read hard drives.</p>
          <p>Seagate drives sizes are 500GB each, however less than 120GB is used on each hard drive.</p>
        </descriptivenote>
      </physdescstructured>
    </physdescset>
    <physloc audience="external">Stored offsite.</physloc>
    <physloc audience="internal" localtype="buillding">ANNEX</physloc>
    <physloc audience="internal" localtype="shelfloc">13:1</physloc>
    <repository>
      <corpname identifier="http://id.quiltlink.org/organizations/9385" source="lcnaf"><part>University Archives</part><part>Piecemaking University</part></corpname>
    </repository>
  </did>
  <accessrestrict>
    <head>Restrictions on Access</head>
    <p>The collection is open for research. The following series require review by the archivist to redact protected information:</p>
    <list listtype="unordered">
      <item>Meeting Notes (for records less than 20 years old)</item>
      <item>Grants</item>
    </list>
  </accessrestrict>
  <accruals>
    <head>Accruals to the Collection</head>
    <p>This collection receives annual accruals of department meeting notes, faculty lists, grants awarded, and annual reports. It receives periodic accruals of recordings of symposia and other hosted events. Grants recordkeeping practices require the department to maintain all documentation for 7 years, after which time it is transferred to the archives.</p>
  </accruals>
  <acqinfo>
    <head>Acquisition History</head>
    <p>Records from 1978 to 1988 were acquired in 1989 from contemporaneous and previous department faculty and the department secretary.</p>
    <p>Materials are transferred annually from the Quilting Technologies department in accordance with University Records Schedule UA-113. Because all files are now electronic, the department retains copies of these records for ongoing reference.</p>
    <p>A 2006 donation of papers and two hard drives by <persname normal="Russel, Martha A. (Martha Anita), 1941-2010"><part>Dr. Martha Russel</part></persname> was used to fill gaps in the record.</p>
  </acqinfo>
  <altformavail>
    <head>Streaming Video</head>
    <p>Recordings of some symposia and colloquia are available on the department's <ref show="new" href="https://youtube.com/quilttech/colloquia" actuate="onload">YouTube page</ref>.</p>
  </altformavail>
  <appraisal>
    <head>Appraisal of Materials</head>
    <p>The initial records collection came to the University Archives as part of a records management initiative in 1989. During processing, University Archivist Wilhelmena Ransom discarded event fliers, duplicate copies of meeting notes, and department publications. Working with the records manager, she drafted the schedule which became schedule UA-113 to guide further materials transfer.</p>
    <p>If records received in an annual transfer (per schedule UA-113) do not fit within existing series, the current University Archivist evaluates them for the potential creation of a new series or subseries.</p>
    <p>Duplicate materials are discarded during processing.</p>
  </appraisal>
  <arrangement>
    <head>Arrangement of the Materials</head>
    <p>Materials are arranged into series and subseries based on contents. Records are arranged chronologically unless otherwise noted.</p>
  </arrangement>
  <bibliography>
    <head>Works Citing the Collection</head>
    <bibref>Collins, Portia. <title><part>"Tools, Techniques, and Technologies: the Evolution of Quilting Practice from 1820 to 2000"</part></title>. Quilting Studies Quarterly 83:2 (<date normal="2013-06">June 2013</date>): 283-312.</bibref>
    <bibref>Gilmore, Marian. <title render="italic"><part>Rethinking Women's Technologies</part></title>. <date normal="1998">1998</date>. <ref href="https://catalog.piecemaking.edu/catalog/9twe2" show="new" actuate="onload">Catalog record.</ref></bibref>
    <bibref>Miller, Marcus. <title render="italic"><part>Teaching Textiles</part></title>. <date normal="2010">2010</date>. <ref href="https://catalog.piecemaking.edu/catalog/93952" show="new" actuate="onload">Catalog record.</ref></bibref>
  </bibliography>
  <bioghist>
    <head>Departmental History</head>
    <p>The Quilting Technologies Department was created in 1978, 3 years after the founding of <corpname><part>Piecemaking University</part></corpname> in order to <quote>study the history of technologies of quilting and promote the exploration and development of new technologies.</quote> Its two full-time faculty members were <persname identifier="http://id.loc.gov/authorities/names/no2019174587" normal="Russel, Martha A. (Martha Anita), 1941-2010"><part>Dr. Martha Russel</part></persname> and <persname identifier="http://id.loc.gov/authorities/names/no2019174587233" normal="Carter, Caroline, 1932-"><part>Dr. Caroline Carter</part></persname>. At the time of its founding, the department only offered a single degree, the Doctorate in Quilting.</p>
    <p>According to historian Amelia Robinson:</p>
    <blockquote><p>...the late 1970s saw Quilt Education to shift from a discipline concerned with history to an interdisciplinary and forward-looking field. No longer confined to studying and describing the textiles themselves or the persons who created them, quilt educators began exploring the techniques and technologies of quilting and its intersection with other textile arts and crafts. Entire departments, such as the Quilting Technologies Department at Piecemaking University, began stitching together connections with women's history, the history of industrialization, the study of technology, and experimental research.<lb/>
    <footnote>
    <p><ptr target="patchwork" show="replace" linktitle="Patchwork: A History of Quilting Education in the United States"/></p>
    </footnote></p>
    </blockquote>
    <p>By 1985, the department had grown to 5 faculty positions and 3 interdisciplinary positions shared with other parts. It began offering three program tracks: textile history, history of technology, and women's history. Around 1987, the department began offering 400-level classes for undergraduates majoring in general history and women's studies.</p>
    <p>In 2000, the department began including courses on digital technologies, although the department's mission continues to define Quilting Technology as "the broad set of processes, skills, tools, and techniques developed for the creation of quilts and related textiles." In 2010, the department partnered with the library in the creation of a makerspace.</p>
  </bioghist>
  <controlaccess>
    <persname identifier="http://id.loc.gov/authorities/names/no2019174587" encodinganalog="600" normal="Russel, Martha A. (Martha Anita), 1941-2010" source="lcnaf"><part localtype="familyname">Russel</part> <part localtype="forename">Martha</part><part localtype="date">1941-2010</part></persname>
    <corpname identifier="http://id.loc.gov/authorities/names/no2019174523526" encodinganalog="610"><part>Quilting Technologies Department</part><part>Piecemaking University</part></corpname>
    <subject encodinganalog="650" source="lcsh"><part identifier="http://id.loc.gov/authorities/subjects/sh85109859">Quilting</part><part identifier="http://id.loc.gov/authorities/subjects/sh99005024">History</part></subject>
    <subject identifier="http://id.loc.gov/authorities/subjects/sh85133169" encodinganalog="650" source="lcsh"><part>Technology</part><part>Social aspects</part></subject>
    <subject identifier="http://id.quiltlink.org/subjects/T29485" encodinganalog="650" source="quiltlink"><part>Technologies of quilting</part></subject>
    <subject identifier="http://id.quiltlink.org/subjects/T2532" encodinganalog="650" source="quiltlink"><part>Quilt study</part></subject>
    <genreform identifier="http://id.loc.gov/authorities/genreForms/gf2014026128" encodinganalog="655" source="lcgft"><part>Minutes (Records)</part></genreform>
    <genreform identifier="http://id.loc.gov/authorities/genreForms/gf2014026046" encodinganalog="655" source="lcgft"><part>Annual reports</part></genreform>
    <genreform identifier="http://id.loc.gov/authorities/genreForms/gf2014026067" encodinganalog="655" source="lcgft"><part>Conference materials</part></genreform>
    <genreform identifier="http://id.quiltlink.org/genres/F23569" encodinganalog="655" source="quiltlink"><part>Grant materials</part></genreform>
    <occupation identifier="http://vocab.getty.edu/page/aat/300025359" encodinganalog="656" source="aat"><part>quiltmakers</part></occupation>  
  </controlaccess>
  <custodhist>
    <head>Custodial History</head>
    <chronlist>
      <chronitem>
        <daterange>
          <fromdate standarddate="1978">1978</fromdate>
          <todate standarddate="1989">1989</todate>
        </daterange>
        <chronitemset>
          <event>Materials held by faculty members.</event>
          <event>Materials collected by departmental secretaries as informal archive</event>        
        </chronitemset>
      </chronitem>
      <chronitem>
        <datesingle>1989</datesingle>
        <event>Departmental Records Collection Initiative conducted by <corpname normal="University Archives, Piecemaking University"><part>University Archives</part></corpname></event>
      </chronitem>
      <chronitem>
        <daterange>
          <fromdate standarddate="1989">1989</fromdate>
          <todate notbefore="2020">2020</todate>
        </daterange>
        <event>Records held by University Archives</event>
      </chronitem>
    </chronlist>
  </custodhist>
  <legalstatus>
    <head>Legal Status of the Materials</head>
    <p>Under copyright.</p>
  </legalstatus>
  <phystech>
    <head>Technical Notes</head>
    <p>Access to hard drives included in the collection require the use of docking station which supports 3.5" SATA 3GB/s drives. Docking stations may be found in the University Archives' reading room.</p>
  </phystech>
  <prefercite>
    <head>Citation</head>
    <p>Cite materials as follows:</p>
    <p>[Description or identification of item], [ date, if known]; Quilting Technologies Department Records (QD011). Piecemaking University Archives, Lancaster, PA 17603.</p>
  </prefercite>
  <processinfo>
    <head>Processing Information</head>
    <p>The original collection was accessioned and processed by University Archivist Wilhelmena Ransom between 1989 and 1990. Accruals are processed annually by student workers with guidance by the University Archivist on occasions when material warranted the creation of a new series.</p>
    <p>In 2006, upon <persname normal="Russel, Martha A. (Martha Anita), 1941-2010"><part>Dr. Martha Russel's</part></persname> transition to emerita status, Digital Archivist Monica Brown accessioned and processed the two hard drives on which Dr. Russel had stored digital meeting notes, colloquia and symposium reccordings, and other miscellaneous departmental materials. These were used to fill gaps in existing series.</p>
    <p>From 2016 to 2020, Electronic Records Archivist María Ramirez has annually accessioned and processed the meeting notes, policies, and annual reports via a Box folder shared with the department's administative assistant. Copies of these born-digital materials are stored on the University Archives' fileserver and linked in the finding aid.</p>
  </processinfo>
  <relatedmaterial>
    <head>Related material</head>
    <archref><title><part>Martha Russel Papers, 1962-2010</part></title>, #<num localtype="collection_id">mr012</num>, <ref href="https://piecemaking.edu/ark:/1313/mr012">view finding aid</ref></archref>
  </relatedmaterial>
  <relations>
    <relation relationtype="cpfrelation" actuate="onload" show="new" href="https://piecemaking.edu/ark:/1313/mr012-cpf.xml">
      <relationentry>Martha Russel</relationentry>
      <daterange>
          <fromdate standarddate="1978">1978</fromdate>
          <todate standarddate="2010">2010</todate>
      </daterange>
      <descriptivenote>
        <p>Department founder and chair from 1978-2001. Professor from 2001-2006. Professor emerita from 2006 until her death in 2010.</p>
      </descriptivenote>
    </relation>
    <relation relationtype="cpfrelation" actuate="onload" show="new" href="https://piecemaking.edu/ark:1313/gqt014">
      <relationentry>Graduate Society of Quilting Technologists</relationentry>
      <daterange>
        <fromdate standarddate="1980">1980</fromdate>
      </daterange>
      <descriptivenote>
        <p>Organized and run by graduate students within the department with a rotating faculty advisor, this society co-sponsors the "Threads and Voices" symposium series. Its students invite speakers, conduct event planning, and host the event, while the department provides financial support and guidance.</p>
      </descriptivenote>
    </relation>
    <relation relationtype="resourcerelation" actuate="onload" show="new" href="https://quilthismus.org/archives/coll/9274">
      <relationentry>Collaborations</relationentry>
      <objectxmlwrap>
        <rdf:Description>
          <dc:title>Collaborations: Quilting History Museum and Piecemaking U</dc:title>
          <dc:identifier>9274</dc:identifier>
          <dc:date>1986-1992</dc:date>
          <dc:subject>exhibits</dc:subject>
          <dc:subject>teaching</dc:subject>
          <dc:subject>quilts</dc:subject>
          <dc:subject>technology</dc:subject>
        </rdf:Description>
      </objectxmlwrap>
      <descriptivenote><p>Exhibit notes and meeting minutes from a project partnership between the Quilting History Museum and department members of the Quilting Technologies Department</p></descriptivenote>
    </relation>
  </relations>
  <scopecontent>
    <head>Scope and Content of the Collection</head>
    <p>The collection contains departmental business records: meeting notes, policies, grants, and annual reports. A selection of syllabuses ranging from 1978-1999 reflect the department's practice of allowing prospective and enrolled students to browse a course's contents before registration. The collection also contains recordings of symposia and colloquia sponsored by the department.</p>
  </scopecontent>
  <separatedmaterial>
    <head>Separated Material</head>
    <p>While the hard drives are retained within this collection, all of <persname normal="Russel, Martha A. (Martha Anita), 1941-2010"><part>Dr. Martha Russel's</part></persname> personal records, (digital and analog) including correspondence, external committee service documents, and other manuscript materials were separated during processing to create the Martha Russel Papers. <ref href="https://piecemaking.edu/ark:/1313/mr012">View finding aid.</ref></p>
    <archref><title><part>Martha Russel Papers, 1962-2010</part></title>, #<num localtype="collection_id">mr012</num></archref>
  </separatedmaterial>
  <userestrict>
    <head>Restrictions on Use of Materials</head>
    <p>Duplication of event recordings may require the permission of their copyright holders.</p>
    <table frame="all" colsep="true" rowsep="true">
      <tgroup align="center" cols="3">
        <colspec colname="materialtype" colnum="1"/>
        <colspec colname="usetype" colnum="2"/>
        <colspec colname="permissionrequired" colnum="3"/>
        <thead valign="middle">
          <row>
            <entry colname="materialtype">Material Type</entry>
            <entry colname="usetype">Use Type</entry>
            <entry colname="permissionrequired">Permission Required</entry>
          </row>
        </thead>
        <tbody valign="middle">
          <row>
            <entry colname="materialtype">Event recordings, 1980-2008</entry>
            <entry colname="usetype">File copying or other duplication of audio or visual content</entry>
            <entry colname="permissionrequired">Requires permission of the speakers or copyright holders as no waivers or permission slips were obtained</entry>
          </row>
          <row>
            <entry colname="materialtype">Event recordings, 2009-</entry>
            <entry colname="usetype">File copying or other duplication of audio or visual content</entry>
            <entry colname="permissionrequired">Consult with the archivist for waivers which contain specific guidance by each speaker for the duplication and use of each recording. Speakers were given the following options: to restrict duplication entirely, to allow duplication for personal research use, to allow duplication for personal research use and teaching, or to freely allow duplication and use of the materials. These permissions were obtained separately from streaming permissions and copies of the video streaming in the university's YouTube channel should not be interpreted as license to duplicate.</entry>
          </row>
        </tbody>
      </tgroup>
    </table>
  </userestrict>
  <dsc>
    <c></c>
    <c></c>
  </dsc>
</archdesc>
```

<h3 id="changes">Changes from EAD 2002</h3>

<archdesc> gained @lang, @localtype, and @script attributes while losing plain @type. It no longer contains <dao> or deprecated child elements <daogrp>, <descgrp>, <note>, and <runner>. It gained <legalstatus> and <relations>.